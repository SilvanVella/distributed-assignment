﻿using DistributedAPI.Rest;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace DistributedAPI.OAuth
{
    public class OAuth
    {
        string ConsumerKey { get; set; }
        string ConsumerKeySecret { get; set; }
        string SignatureMethod { get; set; }
        string AccessToken { get; set; }
        string AccessTokenSecret { get; set; }
        string Version { get; set; }

        public OAuth(string accessToken, string accessTokenSecret)
        {
            ConsumerKey = "0S9ln6nOxZAXEChNM0AhIRbcf";
            ConsumerKeySecret = "Xy4oS4bJQTGAOPJ0nbatGF6XZKSkJiZRQzZ9K9rarwZrtyCZ3B";
            SignatureMethod = "HMAC-SHA1";
            AccessToken = accessToken;
            AccessTokenSecret = accessTokenSecret;
            Version = "1.0";
        }

        private static string CreateOAuthTimestamp()
        {

            var nowUtc = DateTime.UtcNow;
            var timeSpan = nowUtc - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var timestamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();

            return timestamp;
        }

        private string CreateOauthNonce()
        {
            var oauthNonce = Convert.ToBase64String(new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString(CultureInfo.InvariantCulture)));
            return oauthNonce;
        }

        private string CreateOauthSignature(string resourceUrl, httpVerb method, string oauthNonce, string oauthTimestamp, string msg)
        {
            var baseFormat = "oauth_consumer_key={0}&oauth_nonce={1}&oauth_signature_method={2}" +
                            "&oauth_timestamp={3}&oauth_token={4}&oauth_version={5}";

            if (msg != "")
            {
                baseFormat += "&status=" + Uri.EscapeDataString(msg);
            }

            var baseString = string.Format(baseFormat,
                                        ConsumerKey,
                                        oauthNonce,
                                        SignatureMethod,
                                        oauthTimestamp,
                                        AccessToken,
                                        Version
                                        );

            baseString = string.Concat(method.ToString() + "&", Uri.EscapeDataString(resourceUrl), "&", Uri.EscapeDataString(baseString));

            var compositeKey = string.Concat(Uri.EscapeDataString(ConsumerKeySecret),"&", Uri.EscapeDataString(AccessTokenSecret));

            string oauth_signature;
            using (HMACSHA1 hasher = new HMACSHA1(ASCIIEncoding.ASCII.GetBytes(compositeKey)))
            {
                oauth_signature = Convert.ToBase64String(
                    hasher.ComputeHash(ASCIIEncoding.ASCII.GetBytes(baseString)));
            }

            string autht = Uri.EscapeDataString(oauth_signature);
            return autht;
        }

        public string createHeader(string resourceURL, httpVerb method, string msg = "")
        {

            var authNonce = CreateOauthNonce();
            var authTime = CreateOAuthTimestamp();
            var sig = CreateOauthSignature(resourceURL, method, authNonce, authTime, msg);

            StringBuilder b = new StringBuilder();
            b.Append($"OAuth oauth_consumer_key={ConsumerKey},");
            b.Append($"oauth_nonce={authNonce},");
            b.Append($"oauth_signature_method={SignatureMethod},");
            b.Append($"oauth_timestamp={authTime},");
            b.Append($"oauth_token={AccessToken},");
            b.Append($"oauth_version={Version},");
            b.Append($"oauth_signature={sig}");

            var sigBaseString = b.ToString();
            return sigBaseString;
        }
    }
}