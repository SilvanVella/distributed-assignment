﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace DistributedAPI.Rest
{

    public enum httpVerb
    {
        GET,
        POST
    }

    public class RestClient
    {
        public string endpoint { get; set; }
        public httpVerb httpMethod { get; set; }

        public RestClient(httpVerb verb)
        {
            endpoint = string.Empty;
            httpMethod = verb;
        }

        private string retrieveResponse(HttpWebRequest request)
        {

            string strResponseValue = string.Empty;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApplicationException($"Error Code: {response.StatusCode}");
                }

                using (Stream responseStream = response.GetResponseStream())
                {
                    if (responseStream != null)
                    {
                        using (StreamReader reader = new StreamReader(responseStream))
                        {
                            strResponseValue = reader.ReadToEnd();
                        }
                    }
                }
            }

            return strResponseValue;
        }

        public string makeRequest(Dictionary<string, string> headers, string msg = null)
        {

            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(endpoint);

            request.Method = httpMethod.ToString();

            //for each header in our list (dictionary) headers
            foreach (KeyValuePair<string, string> header in headers)
            {
                request.Headers.Add(header.Key, header.Value);
            }

            if (msg != null)
            {
                if(httpMethod == httpVerb.POST)
                {
                    request.ContentType = "application/x-www-form-urlencoded";
                    using (Stream objStream = request.GetRequestStream())
                    {
                        byte[] content = ASCIIEncoding.ASCII.GetBytes("status=" + Uri.EscapeDataString(msg));
                        objStream.Write(content, 0, content.Length);
                    }
                }
            }

            return retrieveResponse(request);
        }

        //this method is going to send the request and return the json result (if found)
        public string makeRequest()
        {
            //string strResponseValue = string.Empty;

            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(endpoint);

            request.Method = httpMethod.ToString();

            return retrieveResponse(request);
        }
    }
}