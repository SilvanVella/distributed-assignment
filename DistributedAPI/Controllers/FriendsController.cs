﻿using DistributedAPI.Endpoints;
using DistributedAPI.Rest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DistributedAPI.Controllers
{
    [Authorize]
    public class FriendsController : ApiController
    {
        public string Get(string id, string arg)
        {

            TwitterAPIEndpoint twitterAPIEndpoint = new TwitterAPIEndpoint();
            OAuth.OAuth oauth = new OAuth.OAuth(id, arg);
            string AuthSign = oauth.createHeader(twitterAPIEndpoint.getFriends(), httpVerb.GET);
            twitterAPIEndpoint.Signature = AuthSign;
            RestClient restClient = new RestClient(httpVerb.GET);
            restClient.endpoint = twitterAPIEndpoint.getFriends();
            return restClient.makeRequest(twitterAPIEndpoint.getEndpoint());
        }
    }
}
