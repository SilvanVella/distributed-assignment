﻿using DistributedAPI.Endpoints;
using DistributedAPI.Rest;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DistributedAPI.Controllers
{
    [Authorize]
    public class FeedController : ApiController
    {
        public string Post([FromBody] JObject id)
        {
            string pageId = id["pageId"].ToString();
            string accessToken = id["access_token"].ToString();

            RestClient restClient = new RestClient(httpVerb.GET);
            FacebookGraphAPIEndpoint facebookGraphAPIEndpoint = new FacebookGraphAPIEndpoint(accessToken);
            facebookGraphAPIEndpoint.endpointType = EndpointType.FEED;
            restClient.endpoint = facebookGraphAPIEndpoint.getPageFeed(pageId);
            return restClient.makeRequest();
        }
    }
}
