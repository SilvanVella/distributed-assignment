﻿using DistributedAPI.Endpoints;
using DistributedAPI.Rest;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DistributedAPI.Controllers
{
    [Authorize]
    public class PostsController : ApiController
    {
        public string Get(string id)
        {
            RestClient restClient = new RestClient(httpVerb.GET);
            FacebookGraphAPIEndpoint facebookGraphAPIEndpoint = new FacebookGraphAPIEndpoint(id);
            facebookGraphAPIEndpoint.endpointType = EndpointType.POSTS;
            restClient.endpoint = facebookGraphAPIEndpoint.getPosts();
            return restClient.makeRequest();
        }
    }
}
