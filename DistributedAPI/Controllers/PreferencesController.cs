﻿using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DistributedAPI.Controllers
{
    [Authorize]
    public class PreferencesController : ApiController
    {
        public object ResultGroups { get; private set; }

        public string Get()
        {
            using (DistributedProgrammingEntities entites = new DistributedProgrammingEntities())
            {
                List<Preference> preferences =  entites.Preferences.ToList();

                string serializedItem = JsonConvert.SerializeObject(preferences, Formatting.Indented,
                new JsonSerializerSettings
                {
                    //PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

                return serializedItem;
            }
        }

        public IHttpActionResult Get(string id)
        {

            if (id == null || id == "" || id == " ")
            {
                return StatusCode(HttpStatusCode.NotModified);
            }

            ConnectionStringSettings mySetting = ConfigurationManager.ConnectionStrings["DefaultConnection"];
            string conString = mySetting.ConnectionString;


            SqlConnection conn = new SqlConnection(conString);

            string userId = User.Identity.GetUserId();

            string[] preferencesArr = id.Split(new string[] { "&" }, StringSplitOptions.None);

            DistributedProgrammingEntities entites = new DistributedProgrammingEntities();

            string deleteSql = "DELETE FROM UserPreferences WHERE UserId = @userId";
            conn.Open();
            SqlCommand deleteCommand = new SqlCommand(deleteSql, conn);
            deleteCommand.Parameters.AddWithValue("@userId", userId);
            deleteCommand.ExecuteNonQuery();
            conn.Close();

            for (int i = 0; i < preferencesArr.Length; i++)
            {
                string preference = preferencesArr[i];
                int preferenceId = entites.Preferences.Where(x => x.Preference1 == preference).Select(x => x.Id).FirstOrDefault();

                string sql = "INSERT INTO UserPreferences(UserId, PreferenceId) VALUES(@userId, @preferenceId)";
                
                try
                {

                    conn.Open();
                    SqlCommand command = new SqlCommand(sql, conn);
                    command.Parameters.AddWithValue("@userId", userId);
                    command.Parameters.AddWithValue("@preferenceId", preferenceId);
                    command.ExecuteNonQuery();
                    conn.Close();

                }
                catch (Exception ex)
                {
                    conn.Close();
                }
            }

            return Ok();
        }
    }
}
