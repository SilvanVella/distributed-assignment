﻿using DistributedAPI.Endpoints;
using DistributedAPI.Rest;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DistributedAPI.Controllers
{
    [Authorize]
    public class ProfileController : ApiController
    {
        public string Get(string id)
        {

            DistributedProgrammingEntities entities = new DistributedProgrammingEntities();
            string userId = User.Identity.GetUserId();

            List <AspNetUser> userPref = entities.AspNetUsers.Where(x => x.Preferences.Any(y => x.Id == userId)).ToList();
            int size = userPref[0].Preferences.Count;
            string fields = "";

            for (int i = 0; i < size; i++)
            {
                fields += userPref[0].Preferences.ElementAt(i).Preference1;

                if (i < size - 1)
                {
                    fields += ",";
                }
            }

            RestClient restClient = new RestClient(httpVerb.GET);
            FacebookGraphAPIEndpoint facebookGraphAPIEndpoint = new FacebookGraphAPIEndpoint(id);
            restClient.endpoint = facebookGraphAPIEndpoint.getProfile(fields);

            return restClient.makeRequest();
        }
    }
}
