﻿using DistributedAPI.Endpoints;
using DistributedAPI.Rest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DistributedAPI.Controllers
{
    [Authorize]
    public class PhotosController : ApiController
    {
        public string Get(string id)
        {
            RestClient restClient = new RestClient(httpVerb.GET);
            FacebookGraphAPIEndpoint facebookGraphAPIEndpoint = new FacebookGraphAPIEndpoint(id);
            facebookGraphAPIEndpoint.endpointType = EndpointType.PHOTOS;
            restClient.endpoint = facebookGraphAPIEndpoint.getPhotos();
            return restClient.makeRequest();
        }
    }
}
