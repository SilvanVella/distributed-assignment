﻿using DistributedAPI.Endpoints;
using DistributedAPI.Rest;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DistributedAPI.Controllers
{
    [Authorize]
    public class CommentsController : ApiController
    {
        public string Post([FromBody] JObject id)
        {

            string postId = id["postId"].ToString();
            string accessTokenPage = id["access_token"].ToString();
            string message = id["message"].ToString();

            if (message == null || message == "" || message == "null")
            {
                return @"{""message"":""Message cannot be left empty""}";
            }

            RestClient restClient = new RestClient(httpVerb.POST);
            FacebookGraphAPIEndpoint facebookGraphAPIEndpoint = new FacebookGraphAPIEndpoint(accessTokenPage);
            facebookGraphAPIEndpoint.endpointType = EndpointType.COMMENTS;
            restClient.endpoint = facebookGraphAPIEndpoint.postComments(postId, message);
            return restClient.makeRequest();
        }
    }
}
