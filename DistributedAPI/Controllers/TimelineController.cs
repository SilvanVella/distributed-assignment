﻿using DistributedAPI.Endpoints;
using DistributedAPI.OAuth;
using DistributedAPI.Rest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;


namespace DistributedAPI.Controllers
{
    [Authorize]
    public class TimelineController : ApiController
    {
        public string Get(string id, string arg)
        {
            TwitterAPIEndpoint twitterAPIEndpoint = new TwitterAPIEndpoint();
            OAuth.OAuth oauth = new OAuth.OAuth(id, arg);
            string AuthSign = oauth.createHeader(twitterAPIEndpoint.getTimeline(), httpVerb.GET);
            twitterAPIEndpoint.Signature = AuthSign;
            RestClient restClient = new RestClient(httpVerb.GET);
            restClient.endpoint = twitterAPIEndpoint.getTimeline();
            return restClient.makeRequest(twitterAPIEndpoint.getEndpoint());
        }

        public string Get(string id, string arg, string msg)
        {

            if (msg == null || msg == "")   
            {
                return @"{""message"":""Message cannot be left empty""}";
            }

            TwitterAPIEndpoint twitterAPIEndpoint = new TwitterAPIEndpoint();
            OAuth.OAuth oauth = new OAuth.OAuth(id, arg);
            string AuthSign = oauth.createHeader(twitterAPIEndpoint.postTweet(), httpVerb.POST, msg);
            twitterAPIEndpoint.Signature = AuthSign;
            RestClient restClient = new RestClient(httpVerb.POST);
            restClient.endpoint = twitterAPIEndpoint.postTweet();
            return restClient.makeRequest(twitterAPIEndpoint.getEndpoint(), msg);
        }
    }
}
