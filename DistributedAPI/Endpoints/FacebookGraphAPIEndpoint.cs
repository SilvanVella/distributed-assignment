﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace DistributedAPI.Endpoints
{
    public class FacebookGraphAPIEndpoint : Endpoint
    {
        public FacebookGraphAPIEndpoint(string accessToken) : base("https://graph.facebook.com/me", accessToken) { }

        public string getPosts()
        {
            StringBuilder stringBuilder = new StringBuilder(baseEndpoint);
            stringBuilder.Append("/");
            stringBuilder.Append(getEndpointType());
            stringBuilder.Append("?");
            stringBuilder.Append("access_token=");
            stringBuilder.Append(accessToken);

            return stringBuilder.ToString();
        }

        public string getLikes()
        {
            StringBuilder stringBuilder = new StringBuilder(baseEndpoint);
            stringBuilder.Append("/");
            stringBuilder.Append(getEndpointType());
            stringBuilder.Append("?");
            stringBuilder.Append("access_token=");
            stringBuilder.Append(accessToken);

            return stringBuilder.ToString();
        }

        public string getProfile(string fields)
        {
            StringBuilder stringBuilder = new StringBuilder(baseEndpoint);
            stringBuilder.Append("?");
            stringBuilder.Append("fields=");
            stringBuilder.Append(fields);
            stringBuilder.Append("&");
            stringBuilder.Append("access_token=");
            stringBuilder.Append(accessToken);

            return stringBuilder.ToString();
        }

        public string getAccounts()
        {
            StringBuilder stringBuilder = new StringBuilder(baseEndpoint);
            stringBuilder.Append("/");
            stringBuilder.Append(getEndpointType());
            stringBuilder.Append("?");
            stringBuilder.Append("access_token=");
            stringBuilder.Append(accessToken);
            return stringBuilder.ToString();
        }

        public string getPhotos()
        {
            StringBuilder stringBuilder = new StringBuilder(baseEndpoint);
            stringBuilder.Append("/");
            stringBuilder.Append(getEndpointType());
            stringBuilder.Append("?");
            stringBuilder.Append("fields=");
            stringBuilder.Append("picture");
            stringBuilder.Append("&");
            stringBuilder.Append("type=");
            stringBuilder.Append("uploaded");
            stringBuilder.Append("&");
            stringBuilder.Append("access_token=");
            stringBuilder.Append(accessToken);

            return stringBuilder.ToString();
        }

        public string getPageFeed(string pageId)
        {
            StringBuilder stringBuilder = new StringBuilder(baseEndpoint.Replace("me", pageId));
            stringBuilder.Append("/");
            stringBuilder.Append(getEndpointType());
            stringBuilder.Append("?");
            stringBuilder.Append("access_token=");
            stringBuilder.Append(accessToken);
            return stringBuilder.ToString();
        }

        public string postComments(string postId, string message)
        {
            StringBuilder stringBuilder = new StringBuilder(baseEndpoint.Replace("me", postId));
            stringBuilder.Append("/");
            stringBuilder.Append(getEndpointType());
            stringBuilder.Append("?");
            stringBuilder.Append("message=");
            stringBuilder.Append(message);
            stringBuilder.Append("&");
            stringBuilder.Append("access_token=");
            stringBuilder.Append(accessToken);
            return stringBuilder.ToString();
        }
    }
}