﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DistributedAPI.Endpoints
{

    public enum EndpointType
    {
        POSTS,
        FEED,
        LIKES,
        PHOTOS,
        ACCOUNTS,
        COMMENTS
    }

    public abstract class Endpoint
    {

        protected string baseEndpoint;
        protected string accessToken;
        protected string fields;

        public EndpointType endpointType { get; set; }

        public Endpoint(string baseEndpoint, string accessToken)
        {
            this.baseEndpoint = baseEndpoint;
            this.accessToken = accessToken;
        }

        protected string getEndpointType()
        {
            switch (endpointType)
            {
                case EndpointType.POSTS:
                    return "posts";
                case EndpointType.FEED:
                    return "feed";
                case EndpointType.LIKES:
                    return "likes";
                case EndpointType.ACCOUNTS:
                    return "accounts";
                case EndpointType.COMMENTS:
                    return "comments";
                case EndpointType.PHOTOS:
                    return "photos";
                default:
                    return "posts";
            }
        }
    }
}